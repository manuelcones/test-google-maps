import React, { Component } from 'react';
import {Map, GoogleApiWrapper} from 'google-maps-react';
import './App.css';

class App extends Component {

  constructor(props) {
      super(props)
      this.state={
        mapProperties:{
            zoom:6,
            center:null
          }
        }


        this.getMapCenter = this.getCenter.bind(this);
    }


  render() {
    return (

      <Map google={this.props.google} zoom={this.state.mapProperties.zoom}  center={this.state.mapProperties.center}   onReady={this.getMapCenter}>


      </Map>
    );
  }

getCenter(){
   let geocoder = new window.google.maps.Geocoder();

   geocoder.geocode( {'address' : 'mexico'}, (results, status)=>{
    if (status == window.google.maps.GeocoderStatus.OK) {
      console.log(results[0].geometry.location);
      this.setState({mapProperties:{ zoom:6,center:results[0].geometry.location }});

    }
});
}

}

export default GoogleApiWrapper({
  apiKey: ('AIzaSyA7Sa4jSQyLDrWZFtlQguhEqWXkXk3FNRA')
})(App)
